RELEASE=3.3

PACKAGE=parted
PKGVERSION=3.2
PKGRELEASE=6
BPORELEASE=~bpo70+1

PKGDIR=${PACKAGE}-${PKGVERSION}
PKGSRC=${PACKAGE}_${PKGVERSION}.orig.tar.xz
DEBSRC=${PACKAGE}_${PKGVERSION}-${PKGRELEASE}.debian.tar.xz

ARCH:=$(shell dpkg-architecture -qDEB_BUILD_ARCH)

DEBS=							\
${PACKAGE}_${PKGVERSION}-${PKGRELEASE}${BPORELEASE}_${ARCH}.deb	\
libparted2_${PKGVERSION}-${PKGRELEASE}${BPORELEASE}_${ARCH}.deb


all: ${DEBS}
	echo ${DEBS}

${DEBS}: ${PKGSRC} changelog.pve
	rm -rf ${PKGDIR} debian
	tar xf ${PKGSRC}
	tar xf ${DEBSRC}
	cp -a debian ${PKGDIR}/debian
	mv ${PKGDIR}/debian/changelog ${PKGDIR}/debian/changelog.org
	cat changelog.pve ${PKGDIR}/debian/changelog.org > ${PKGDIR}/debian/changelog
	cd ${PKGDIR}; dpkg-buildpackage -rfakeroot -b -us -uc

.PHONY: upload
upload: ${DEBS}
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o rw
	mkdir -p /pve/${RELEASE}/extra
	rm -f /pve/${RELEASE}/extra/Packages*
	rm -f /pve/${RELEASE}/extra/${PACKAGE}_*.deb
	rm -f /pve/${RELEASE}/extra/libparted2_*.deb
	cp ${DEBS} /pve/${RELEASE}/extra
	cd /pve/${RELEASE}/extra; dpkg-scanpackages . /dev/null > Packages; gzip -9c Packages > Packages.gz
	umount /pve/${RELEASE}; mount /pve/${RELEASE} -o ro

distclean: clean

.PHONY: clean
clean:
	rm -rf *~ debian *_${ARCH}.deb *_all.deb *.udeb *.changes *.dsc ${PKGDIR}

.PHONY: dinstall
dinstall: ${DEB}
	dpkg -i ${DEBS}
